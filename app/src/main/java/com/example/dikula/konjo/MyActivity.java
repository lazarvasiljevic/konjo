package com.example.dikula.konjo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;


public class MyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        try {
            add_high_score_online();
            //AddHighscore.doInBackground(getApplicationContext());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.i("ONLINE MOVES", "PAUSE PAUSE PAUSE PAUSE PAUSE");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void StartGame(View arg0) {
        Intent intent = new Intent(this, StartGame.class);
        startActivity(intent);
        //finish();
    }


    public void Highscore(View view) {
        Intent intent = new Intent(this, Highscore.class);
        startActivity(intent);
        //onBackPressed();
        //finish();
    }

    public void ExitGame(View view) {
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }

    public void add_high_score_online(){
        String file = "my_high_score";
        String moves = "0";
        String time = "0";

        String highscore = LocalStorage.get_highscore_local(getApplicationContext());
        if (highscore != null) {
            String[] splitValue = String.valueOf(highscore).split(",");
            moves = splitValue[0];
            time = splitValue[1];
        }

        String username = StartGame.getUsername(getApplicationContext());
        String url = String.format("http://198.211.126.94/konjo/get_player_highscore.php?name=%s", username);
        Log.i("ONLINE MOVES", String.valueOf(url));

        String online_moves = ConnectionToServer.get_response_from_url(url);
        //Log.i("ONLINE MOVES", String.valueOf(Integer.parseInt(online_moves.toString())));
//        if (moves==null){
//            moves="0";
//        }
//        if (online_moves==null){
//            online_moves="0";
//        }
//        if (!moves.equals("0")){
//            Log.i("++++++++++++++++++++", String.valueOf(Integer.parseInt(moves.toString())));
//            Log.i("++++++++++++++++++++", String.valueOf((online_moves)));
//            if (Integer.valueOf(online_moves) > Integer.valueOf(moves)) {
//                deleteFile(file);
//            }else{
//                String new_highscore = ConnectionToServer.add_highscore(username, Integer.valueOf(moves));
//                if (new_highscore != null) {
//                    if (new_highscore.equals("Successfully_addedn")) {
//                        deleteFile(file);
//                    }
//                }
//            }
//        }
    }
}


