package com.example.dikula.konjo;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class StartGame extends AppActivity{

    private Boolean btn_pressed = true;
    private Boolean add_first_button = Boolean.FALSE;
    private Boolean add_second_button = Boolean.FALSE;
    private Button first_button_presed;
    private Button second_button_presed;
    private int first_button;
    private int second_button;
    boolean valid_move = false;
    boolean start = false;
    boolean next_move = false;
    private int moves = 0;
    private Integer number_of_moves=32;
    static TextView moves_text;
    static TextView moves_time;
    TimerThread timer;// = new TimerThread(this);
    Handler handler = new Handler();
    Thread timerThreadHolder;// = new Thread();
    private Timer t;
    private int TimeCounter = 0;
    private int timer_value = 0;
    boolean paused=false;
    boolean pause_true=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_game);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        drawTable();

    }

    @Override
    public void onBackPressed() {
        paused=true;
        pause_game();
    }

    @Override
    protected void onResume(){
        super.onPause();
        paused=false;
        pause_true=true;
    }

    @Override
    protected void onPause(){
        super.onPause();
        paused=true;
    }

//    @Override
//    protected void onStop(){
//        paused=true;
//    }

    public void drawTable() {
        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        String color;

        int screen_width = size.x;
        int screen_height = size.y;
        int button_width = screen_width / 4;
        int button_heght = screen_height / 9;
        int button_id = 0;

        RelativeLayout relativelayout = new RelativeLayout(this);
        RelativeLayout relative1 = new RelativeLayout(this);

        relativelayout.addView(relative1);

        final Map<String, String> dictionary = new HashMap<String, String>();

        final List<Integer> populated_fields = new ArrayList<Integer>();

        for (int c = 0; c < 8; c++) {
            for (int r = 0; r < 4; r++) {
                RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                final CustomButton myButton = new CustomButton(this);

                color = get_color_field(c, r);

                if (color.equals("GRAY")) {
                    myButton.setBackgroundResource(R.drawable.peon_gray);
                }
                else{
                    myButton.setBackgroundResource(R.drawable.peon_white);

                }

                myButton.setId(button_id);

                myButton.buttonCordinates.setXb(c);
                myButton.buttonCordinates.setYb(r);
                myButton.buttonCordinates.setId(button_id);

                String cordinates = String.valueOf(c) + ',' + String.valueOf(r);
                dictionary.put(String.valueOf(button_id), cordinates);

                rel_btn.width = button_width;
                rel_btn.height = button_heght;
                rel_btn.leftMargin = button_width * r;
                rel_btn.topMargin = button_heght * c;

                // add button view
                myButton.setLayoutParams(rel_btn);

                myButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (!valid_move) {
                            next_move = false;
                        }
                        Log.i("DICTIONARYYYYYY!!!!!!!", String.valueOf(dictionary));


                        if (btn_pressed) {
                            first_button = myButton.getId();

                            if (start) {
                                Log.i("FIRST MOVE MOVE MOVE", String.valueOf(dictionary));
                                String value = dictionary.get(String.valueOf(second_button));
                                String[] splitValue = String.valueOf(value).split(",");
                                String valueX = splitValue[0];
                                String valueY = splitValue[1];

                                valid_move = btn_DragDrop(myButton.buttonCordinates.getXb(),
                                        myButton.buttonCordinates.getYb(),
                                        Integer.parseInt(valueX.toString()),
                                        Integer.parseInt(valueY.toString()));
                            }

                            if (valid_move){
                                Log.i("FIRST MOVE MOVE MOVE123", String.valueOf(dictionary));
                                Log.i("USO U VALID!!!!!!!", String.valueOf(timer));


                            }

                            t = new Timer();
                            t.scheduleAtFixedRate(new TimerTask() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    runOnUiThread(new Runnable() {
                                        public void run() {

                                            if (!paused) {
                                                Log.i("TIMER VALUE", String.valueOf(timer_value));
                                                if(pause_true){
                                                    TimeCounter=timer_value;
                                                    timer_value=0;
                                                    pause_true=false;
                                                    Log.i("TIMER VALUE 2", String.valueOf(timer_value));

                                                }

                                                Log.i("TIME TIME TIME", String.valueOf(TimeCounter));
                                                //tvTimer.setText(String.valueOf(TimeCounter)); // you can set it to a textView to show it to the user to see the time passing while he is writing.
                                                TimeCounter++;
                                                timer_value = TimeCounter;
                                            }else{
                                                t.cancel();
                                            }
                                        }
                                    });

                                }


                            }, 0, 10000);

                            if (valid_move || !start) {
//                                Log.i("FIRST MOVE MOVE MOVE1234", String.valueOf(dictionary));
//
//                                Log.i("timerThreadHolder", String.valueOf(timerThreadHolder));
//                                if (timerThreadHolder == null) {
//                                    Log.i("123123123", String.valueOf(timer));
//                                    if(timer == null)
//                                        timer = new TimerThread(StartGame.this);
//                                        //timer = new TimerThread(this);
//
//                                    timerThreadHolder = new Thread(timer);
//                                    Log.i("USO U VALID!!!!!!!", String.valueOf(timer));
//                                    handler.post(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Log.i("USO U VALID!!!!!!!", String.valueOf(timer));
//                                            timer.setRunning(true);
//                                            timerThreadHolder.start();
//                                            Log.i("TIMER TIMER TIMER", String.valueOf(timer.toString()));
//                                        }
//                                    });
//                                } else {
//                                    timer.resume();
//                                }

                                second_button_presed = (Button) findViewById(second_button);
                                int second_button_id = second_button_presed.getId();
                                String original_color = get_button_collor(second_button_id);
                                if (moves > 0) {
                                    second_button_presed.setBackgroundColor(Color.parseColor(original_color));
                                }

                                add_second_button = Boolean.TRUE;

                               String value = dictionary.get(String.valueOf(first_button));
                                String[] splitValue = String.valueOf(value).split(",");
                                String valueX = splitValue[0];
                                String valueY = splitValue[1];

                                String color1 = get_color_field(Integer.parseInt(valueX.toString()), Integer.parseInt(valueY.toString()));
                                if (color1 == "GRAY") {
                                    myButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.horse_gray));
                                }
                                else {
                                    myButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.horse_white));
                                }

                                btn_pressed = false;
                                next_move = true;
                                //if (moves != 0) {
                                moves += 1;
                                Log.i("ELSE USOOOOOOO", Integer.toString(myButton.getId()));
                                //}
                                Log.i("ELSE USOOOOOOO", Integer.toString(myButton.getId()));
                                if (!populated_fields.contains(myButton.getId())) {
                                    populated_fields.add(myButton.getId());
//                                    save_board(populated_fields);
//                                    String get_local_high_score = LocalStorage.get_highscore_local(getApplicationContext());
//                                    Log.i("IDEMO MALENA MALENA", get_local_high_score);
//                                    List<String> list = new ArrayList<String>(Arrays.asList(get_local_high_score.split(" , ")));
//                                    boolean exist = list.contains(9);
//                                    Log.i("IDEMO IDEMO IDEMO MALENA", String.valueOf(exist));


                                }
                                if (populated_fields.size() == number_of_moves) {
                                    end_game(moves);
                                }
                                Log.i("populated_fields?????!!!!!!!", String.valueOf(populated_fields));
                            }
                        } else {
                            first_button_presed = (Button) findViewById(first_button);
                            int first_button_id = first_button_presed.getId();

                            Log.i("GET FIRST BUTTON", Integer.toString(first_button_id));
                            Log.i("BUTTON PRESSED", Integer.toString(myButton.getId()));
                            second_button = myButton.getId();
                            if (start) {

                                String value = dictionary.get(String.valueOf(first_button));
                                String[] splitValue = String.valueOf(value).split(",");
                                String valueX = splitValue[0];
                                String valueY = splitValue[1];

                                valid_move = btn_DragDrop(myButton.buttonCordinates.getXb(),
                                        myButton.buttonCordinates.getYb(),
                                        Integer.parseInt(valueX.toString()),
                                        Integer.parseInt(valueY.toString()));
                            }

                            if (valid_move){
                                Log.i("USO U VALID!!!!!!!", String.valueOf(timer));

                                if (timerThreadHolder == null) {
                                    //timer = new TimerThread(this);
                                    if(timer == null)
                                        timer = new TimerThread(StartGame.this);
                                    timerThreadHolder = new Thread(timer);
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            timer.setRunning(true);
                                            timerThreadHolder.start();
                                            //Log.i("TIMER TIMER TIMER", String.valueOf(timer));
                                        }
                                    });
                                } else {
                                    timer.resume();
                                }
                            }

                            if (valid_move || !start) {
                                String original_color = get_button_collor(first_button_id);
                                first_button_presed.setBackgroundColor(Color.parseColor(original_color));

                                add_first_button = Boolean.TRUE;

                                String value = dictionary.get(String.valueOf(second_button));
                                String[] splitValue = String.valueOf(value).split(",");
                                String valueX = splitValue[0];
                                String valueY = splitValue[1];

                                String color1 = get_color_field(Integer.parseInt(valueX.toString()), Integer.parseInt(valueY.toString()));
                                if (color1 == "GRAY") {
                                    myButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.horse_gray));
                                }
                                else {
                                    myButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.horse_white));
                                }

                                btn_pressed = true;
                                next_move = true;
                                moves += 1;
                                Log.i("ELSE USOOOOOOO", Integer.toString(myButton.getId()));
                                if (!populated_fields.contains(myButton.getId())) {
                                    populated_fields.add(myButton.getId());
                                    //save_board(populated_fields);
                                }
                                if (populated_fields.size() == number_of_moves) {
                                    end_game(moves);
                                }
                            }
                        }

                        moves_text.clearComposingText();
                        moves_text.setText("Number of moves: " + moves);

//                        moves_time.clearComposingText();
//                        moves_time.setText("Time: " + timer_value);

//                        String text1 = moves_text.getText().toString();
//                        Log.i("GET TEXT2222 !@#", text1);

                        //Log.i("MOVESSSSSSS", Integer.toString(moves));
                        //Log.i("STARTTTTTTTTTTTTTTT", Boolean.toString(start));
                        //String name = getUsername();

                        if (!start) {
                            start = true;
                        }

                    }
                });

                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);

                moves_text = new TextView(getApplicationContext());
                moves_text.setLayoutParams(lp);
                moves_text.setText("Number of moves: ");
                moves_text.setTextColor(Color.WHITE);
                moves_text.setGravity(Gravity.BOTTOM);
                moves_text.setPadding(button_heght / 3, 0, 0, button_heght / 3);//left,top,right,bottom
                relative1.addView(moves_text);

//                moves_time = new TextView(getApplicationContext());
//                moves_time.setLayoutParams(lp);
//                moves_time.setText("Time: ");
//                moves_time.setTextColor(Color.WHITE);
//                moves_time.setGravity(Gravity.BOTTOM);
//                moves_time.setPadding(button_heght * 3, 0, 0, button_heght / 3);//left,top,right,bottom
//                relative1.addView(moves_time);

                if (add_first_button && valid_move) {
                    relative1.addView(first_button_presed);
                }

                if (add_second_button && valid_move) {
                    relative1.addView(second_button_presed);
                }

                relative1.addView(myButton);
                button_id += 1;

            }
        }

        // Set relative view
        this.setContentView(relativelayout);

    }

    public String get_color_field(int c, int r) {
        String color;
        if (c % 2 == 0) {
            if (r % 2 == 0) {
                color = "WHITE";
            } else {
                color = "GRAY";
            }
        } else {
            if (r % 2 == 0) {
                color = "GRAY";
            } else {
                color = "WHITE";
            }
        }
        return color;
    }

    public String get_button_collor(int id) {
        int[] multiply = new int[]{2, 3, 2, 1};
        Boolean break_loop = Boolean.FALSE;
        Integer number = 0;

        while (number <= id) {
            for (int i : multiply) {
                number += i;

                if (number >= id) {
                    break_loop = Boolean.TRUE;
                    break;
                }
            }
            if (break_loop) {
                break;
            }
        }

        if (id == number || id == 0) {
            return "WHITE";
        } else {
            return "GRAY";
        }
    }

    private Integer get_column(int id, int count) {
        while (id == count) {
            id -= 4;
        }
        return id;
    }

    public Boolean btn_DragDrop(int x, int y, int old_x, int old_y) {
        if (old_x + 2 == x && old_y + 1 == y) {
            return true;
        } else if (old_x + 2 == x && old_y - 1 == y) {
            return true;
        } else if (old_x + 1 == x && old_y + 2 == y) {
            return true;
        } else if (old_x + 1 == x && old_y - 2 == y) {
            return true;
        } else if (old_x - 2 == x && old_y + 1 == y) {
            return true;
        } else if (old_x - 2 == x && old_y - 1 == y) {
            return true;
        } else if (old_x - 1 == x && old_y + 2 == y) {
            return true;
        } else if (old_x - 1 == x && old_y - 2 == y) {
            return true;
        }

        return false;
    }


    private void end_game(Integer moves) {
        String name = getUsername(getApplicationContext());
//        String phone_type = getDeviceName();
//        if (phone_type == "motorola Nexus 6"){
//            Log.i("PHONE TYPE!!!!!!!!!!!!!!!!!!!!$$$$$$$$$$$$$$$", getDeviceName());
//            moves = moves - 1;
//        }

        // Connect to server to submit highscore
        String new_highscore = ConnectionToServer.add_highscore(name, moves, timer_value);
        // If fail connection add highscore to local storage

        String highscore_json = String.format("%s,%s", moves, "12");
        LocalStorage.save_highscore_local(String.valueOf(highscore_json), getApplicationContext());
                //"my_high_score");

        if (new_highscore != "Successfully_addedn") {
            Log.i("NAME NAME NAME", String.valueOf(new_highscore));

//            String highscore_json = String.format("{'name':%s}",name,moves);
            String moves_time = String.format("%s,%s",moves,"12");
            String get_local_high_score = LocalStorage.get_highscore_local(getApplicationContext());

            Log.i("PRE PRVOG IF", get_local_high_score);
            if (get_local_high_score != null) {

                String[] splitValue = get_local_high_score.split(",");

                String old_moves = splitValue[0];
                String old_time = splitValue[1];

                Log.i("PRVI IF", old_moves);
                if (Integer.parseInt(old_moves) > moves) {
                    Log.i("DRUGI IF", String.valueOf(moves_time));
                    LocalStorage.save_highscore_local(String.valueOf(moves_time),
                            getApplicationContext());//, "my_high_score");
                }
            } else {
                Log.i("ELSE ELSE", String.valueOf(moves_time));
                LocalStorage.save_highscore_local(String.valueOf(moves_time),
                        getApplicationContext());//, "my_high_score");
            }
        }
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("End Game");
        alertDialog.setMessage("Moves: " + String.valueOf(moves) + "   Time: " + String.valueOf(timer_value));
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                exit_game();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Restart", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                restart_game();
            }
        });

        alertDialog.show();
    }

    private void pause_game(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Pause Game");
        //alertDialog.setMessage("Your moves:" + String.valueOf(moves));
        alertDialog.setMessage("Moves: " + String.valueOf(moves) + "   Time: " + String.valueOf(timer_value) + " sec");
        alertDialog.setCanceledOnTouchOutside(false);

        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                exit_game();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "Continue", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                paused=false;
                pause_true=true;
                dialog.dismiss();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Restart", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                restart_game();
            }
        });

        alertDialog.show();
    }



    private void exit_game() {
        Intent intent = new Intent(this, MyActivity.class);
        startActivity(intent);
        finish();
    }

    private void restart_game() {
        Intent intent = new Intent(this, StartGame.class);
        startActivity(intent);
        finish();
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return (model);
        } else {
            return (manufacturer) + " " + model;
        }
    }

    public static String getUsername(Context ctx) {
        AccountManager manager = AccountManager.get(ctx);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<String>();

        for (Account account : accounts) {
            // TODO: Check possibleEmail against an email regex or treat
            // account.name as an email address only for certain account.type values.
            possibleEmails.add(account.name);
        }

        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            String email = possibleEmails.get(0);
            String[] parts = email.split("@");

            if (parts.length > 1)
                return parts[0];
        }
        return null;
    }

//    public void save_board(List<Integer> populated_fields){
//        LocalStorage.save_highscore_local(String.valueOf(populated_fields), getApplicationContext(),
//                "draw_board");
//    }

//    public void add_highscore(String player_name, Integer moves, Integer time) {
//        //String address = "http://198.211.126.94/konjo/add_record.php?new_record&player_id=123&moves=10&time=124";
//        //String address = String.format("http://198.211.126.94/konjo/add_record.php?new_record&player_id=%d&moves=%d&time=124", player_name, moves);
//
//        Log.i("NAME NAME NAME", String.valueOf(player_name));
//        HttpClient httpClient = new DefaultHttpClient();
//        // replace with your url
//        HttpPost httpPost = new HttpPost("http://198.211.126.94/konjo/add_record.php?new_record");
//        Log.i("NAME NAME NAME", String.valueOf(player_name));
//
//        //Post Data
//        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
//        nameValuePair.add(new BasicNameValuePair("player_name", player_name));
//        nameValuePair.add(new BasicNameValuePair("moves", String.valueOf(moves)));
//        nameValuePair.add(new BasicNameValuePair("time", String.valueOf(21)));
//        Log.i("NAME NAME NAME", String.valueOf(player_name));
//
//        //Encoding POST data
//        try {
//            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
//        } catch (UnsupportedEncodingException e) {
//            // log exception
//            Log.i("NAME NAME NAME123123", String.valueOf(player_name));
//
//            e.printStackTrace();
//        }
//        Log.i("nameValuePair", String.valueOf(nameValuePair));
//        Log.i("POST POST POST", String.valueOf(httpPost));
//        //making POST request.
//        try {
//            HttpResponse response = httpClient.execute(httpPost);
//            // write response to log
//            Log.i("Http Post Response:", response.toString());
//        } catch (ClientProtocolException e) {
//            // Log exception
//            Log.i("NAME NAME _+_+_+_+", String.valueOf(player_name));
//
//            e.printStackTrace();
//        } catch (IOException e) {
//            // Log exception
//            Log.i("NAME NAME NAME qweewqqweewq", String.valueOf(player_name));
//
//            e.printStackTrace();
//        }
//    }
//
//    public static StringBuilder readFromLink(String url) {
//
//        Log.i("URL URL URL", String.valueOf(url));
//        StringBuilder builder = null;
//        Log.i("URL URL URL", String.valueOf(url));
//        HttpClient client = new DefaultHttpClient();
//        Log.i("URL URL URL", String.valueOf(url));
//        HttpGet httpGet = new HttpGet(url);
//        Log.i("URL URL URL", String.valueOf(url));
//        try {
//            HttpResponse response = client.execute(httpGet);
//            Log.i("RESPONSE", String.valueOf(response));
//            StatusLine statusLine = response.getStatusLine();
//            Log.i("STATUS LINE", String.valueOf(statusLine));
//            int statusCode = statusLine.getStatusCode();
//            Log.i("STATUS CODE", String.valueOf(statusCode));
//            if (statusCode == 200) {
//                HttpEntity entity = response.getEntity();
//                InputStream content = entity.getContent();
//                BufferedReader reader = new BufferedReader(
//                        new InputStreamReader(content), 8192);
//                String line;
//                builder = new StringBuilder();
//                while ((line = reader.readLine()) != null) {
//                    Log.i("LINE LINE LINE", String.valueOf(line));
//                    builder.append(line);
//                }
//            }
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//            return null;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        } catch (Exception e){
//            e.printStackTrace();
//            return null;
//        }
//        return builder;
//    }
//
//    public void get_highscore_list() {
//        BufferedReader in = null;
//        String data = null;
//
//        try{
//            HttpClient httpclient = new DefaultHttpClient();
//
//            HttpGet request = new HttpGet();
//            URI website = new URI("http://198.211.126.94/konjo/get_record.php");
//            request.setURI(website);
//            HttpResponse response = httpclient.execute(request);
//            in = new BufferedReader(new InputStreamReader(
//                    response.getEntity().getContent()));
//
//            // NEW CODE
//            String line = in.readLine();
//            Log.i("LINE LINE LINE +_+_+_+_+_+_+", String.valueOf(line));
//
//        }catch(Exception e){
//            Log.e("log_tag", "Error in http connection "+e.toString());
//        }
//    }
//
//    public void get_text(){
//        try
//        {
//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httppost = new HttpPost(website);
//
//            // no idea what this does :)
//            //httppost.setEntity(new UrlEncodedFormEntity(postParameters));
//
//            // This is the line that send the request
//            HttpResponse response = httpclient.execute(httppost);
//
//            HttpEntity entity = response.getEntity();
//
//            InputStream is = entity.getContent();
//        }
//        catch (Exception e)
//        {
//            Log.e("log_tag", "Error in http connection "+e.toString());
//        }
//    }
}
